const express = require('express');
const app = express();
const ejs = require('ejs');
const path = require('path');
const router = require('./router');

app.set('views', path.join(__dirname,'/views/',))

//engine
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//router
app.use('/', router);

//static file 
app.use(express.static(path.join(__dirname, 'views')));

const port = process.env.PORT || 80

var server = app.listen(port, function(err){
	if(err){
		return console.log('a ocurrido un error'),process.exit(1);
	}
	console.log(`api restful corriendo http//localhost:${port}`);
	})

module.exports = app;	