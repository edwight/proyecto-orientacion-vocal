const express = require('express');
const app = express.Router();

app.get('/',function(req,res){
	return res.render('pages/indexs');
})

let URL = [
	'instrumento-1',
	'instrumento-2',
	'instrumento-3',
	'instrumento-4'
	];


app.get('/'+URL[0]+'/vaccai',function(req,res){
	return res.render('pages/vaccai0');
})
app.get('/'+URL[0]+'/concone',function(req,res){
	return res.render('pages/concone0');
})

app.get('/'+URL[1]+'/vaccai',function(req,res){
	return res.render('pages/vaccai1');
})

app.get('/'+URL[1]+'/concone',function(req,res){
	return res.render('pages/concone1');
})

app.get('/'+URL[2]+'/vaccai',function(req,res){
	return res.render('pages/vaccai2');
})
app.get('/'+URL[2]+'/concone',function(req,res){
	return res.render('pages/concone2');
})
app.get('/'+URL[2]+'/italiana',function(req,res){
	return res.render('pages/italiana0');
})

app.get('/'+URL[3]+'/vaccai',function(req,res){
	return res.render('pages/vaccai3');
})
app.get('/'+URL[3]+'/concone',function(req,res){
	return res.render('pages/concone3');
})
app.get('/'+URL[3]+'/italiana',function(req,res){
	return res.render('pages/italiana1');
})

app.get('/'+URL[0]+'/sojo',function(req,res){
	return res.render('pages/sojo0');
})
app.get('/'+URL[1]+'/sojo',function(req,res){
	return res.render('pages/sojo1');
})
app.get('/'+URL[2]+'/sojo',function(req,res){
	return res.render('pages/sojo2');
})
app.get('/'+URL[3]+'/sojo',function(req,res){
	return res.render('pages/sojo3');
})

app.get('/login',function(req,res){
	return res.render('pages/login');
})
app.get('/admin',function(req,res){
	return res.render('admin/inicio');
})
app.get('/admin/create',function(req,res){
	return res.render('admin/create');
})
app.get('/admin/store',function(req,res){
	console.log(req.body);
})
module.exports = app;