Vue.component('card-post', {
  props: ['title','body','video','video2','link'],
  template: `
    <div class="row">
      <div class="col-md-12">
        <figure class="card card-product">
          <div class="img-wrap embed-container">
            <iframe width="560" height="315" v-bind:src="'https://www.youtube.com/embed/'+ video" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          <figcaption class="info-wrap">
            <a v-if="link" v-bind:href="link" class="float-right">
              <img src="/assets/img/Pdf_icon.png" width="60" height="80">
            </a>
            <a v-if="video2" class="float-right">
              <iframe width="180" height="100" v-bind:src="'https://www.youtube.com/embed/'+ video2" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </a>
            <h4 class="title">{{title}}</h4>
            <p class="desc">{{body}}</p>
              
          </figcaption>
          <div class="bottom-wrap"> 

          </div> <!-- bottom-wrap.// -->
        </figure>
      </div>
    </div>
  `
})
Vue.component('vue-nav', {
  template: `
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">
    <img src="/assets/img/logo-unefm.png" width="130">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/">Inicio<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Instrumento I
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/instrumento-1/vaccai">Vaccai</a>
          <a class="dropdown-item" href="/instrumento-1/concone">Concone</a>
          <a class="dropdown-item" href="/instrumento-1/sojo">Sojo</a>
        </div>
      </li>
     <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Instrumento II
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/instrumento-2/vaccai">Vaccai</a>
          <a class="dropdown-item" href="/instrumento-2/concone">Concone</a>
          <a class="dropdown-item" href="/instrumento-2/sojo">Sojo</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Instrumento III
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/instrumento-3/vaccai">Vaccai</a>
          <a class="dropdown-item" href="/instrumento-3/concone">Concone</a>
          <a class="dropdown-item" href="/instrumento-3/italiana">24 canciones italianas</a>
          <a class="dropdown-item" href="/instrumento-3/sojo">Sojo</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Instrumento IV
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/instrumento-4/vaccai">Vaccai</a>
          <a class="dropdown-item" href="/instrumento-4/concone">Concone</a>
          <a class="dropdown-item" href="/instrumento-4/italiana">24 canciones italianas</a>
          <a class="dropdown-item" href="/instrumento-4/sojo">Sojo</a>
        </div>
      </li>
      
    </ul>
  </div>
</nav>
  `
})
Vue.component('card-sojo', {
  props: ['title','link'],
  template: `
    <a v-bind:href="link" class="list-group-item list-group-item-action">
      <img src="/assets/img/Pdf_icon.png"><p>{{title}}</p>
    </a>
  `
})

Vue.component('vue-header', {
  props: {
    body: String,
    img: String
  },
  template: `
  <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark" v-bind:style="{ 'background-image': 'url(/assets/img/' + img + ')'}" style="background-size: cover; min-height: 500px;">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">Orientación Vocal UNEFM </h1>
      <p class="lead my-3">{{ body }}  </p>
      
    </div>
  </div>
  `
})

Vue.component('vue-aside', {
  props: {
    img: String
  },
  template: `
    <aside class="col-md-4 blog-sidebar">
  <div class="p-3 mb-3 bg-light rounded">
    <h4 class="font-italic">Acerca del Sitio web</h4>
    <p class="mb-0">orientacion-vocal.herokuapp.com es una plataforma de divulgación de contenidos sobre la carrera de canto. un espacio para compartir conocimientos y ser un centro de referencia para los profesiones, estudiantes y cualquier persona interesada en conocer</p>
  </div>
  <div class="p-3">
        <img :src="'/assets/img/' + img" style="width: 100%">
      </div>
  <div class="p-3">
    <vue-recursos></vue-recursos>
  </div>


</aside><!-- /.blog-sidebar -->
  `
})
Vue.component('vue-recursos', {
  template: `
    <div id="recursos">
      <h4 class="font-italic">Recursos</h4>
      <ol class="list-unstyled mb-0">
        <li><a href="https://www.youtube.com/watch?v=ClJYUH3cshY">El Aparato Fonador</a></li>
        <li><a href="https://www.youtube.com/watch?v=DcEwcTGrohw">Como Afinar La Voz</a></li>
        <li><a href="https://www.youtube.com/watch?v=A4Bh8-Eu-WM">NOTAS ALTAS Sin Forzar la Garganta</a></li>     
      </ol>
    </div>
  `
})

Vue.component('vue-card', {
  props: {
    url: String,
    texto: String,
    img:String
  },
  template: `
<div class="row mb-2">
  <div class="col-md-12">
    <div class="card flex-md-row mb-4 shadow-sm h-md-250">
      <div class="card-body d-flex flex-column align-items-start" >
        <strong class="d-inline-block mb-2 text-primary">Archivo</strong>
        <h3 class="mb-0">
          <a class="text-dark" :href="url">Metodo completo</a>
        </h3>
        <p class="card-text mb-auto">{{texto}}</p>
        <a :href="url">Ver Contenido</a>
      </div>
      <img class="card-img-right flex-auto w-50 d-none d-md-block" :src="'/assets/img/'+ img " alt="Card image cap">
    </div>
  </div>

</div>
`})

var app = new Vue({
  el: '#app',
  data: {

    items_vaccai: [
      { 
        id:1,
        title: 'VACCAI lección 1',
        body:'La Escala Diatónica.',
        video:'P_0Zavhs7_Q',
        link:'https://drive.google.com/file/d/1EwdEDJxKC0aBM5nLcLEl_B9EssZ16_b_/view?usp=drive_open'
      },
      { 
        id:2,
        title: 'VACCAI lección 2',
        body:'Intervalos de Tercera.',
        video:'K7YubPrhZ2k',
        link:'https://drive.google.com/file/d/1WzAzLcmNzM9hKFLLv9c8DbH6SvYmBnLI/view'
      },
      { 
        id:3,
        title: 'VACCAI lección 3',
        body:'Intervalos de Cuarta.',
        video:'8WnmeioJpzo',
        link:'https://drive.google.com/file/d/1Ku3BVBP58BJ1mLUbgjPK0WPmWC9DdhNX/view'
      },
      { 
        id:4,
        title: 'VACCAI lección 4',
        body:'Intervalos de Quinta.',
        video:'wAZzjj79Pko',
        link:'https://drive.google.com/open?id=1KOXNs4ybdbicssBPMnaJntWrEuHDP9Mm'
      },
      { 
        id:5,
        title: 'VACCAI lección 5',
        body:'Intervalos de Sexta.',
        video:'y-nNH8X-2-g',
        link:'https://drive.google.com/open?id=1sBL1aTm4enfCXvMWFT7Y0OeE0LJFnY5x'
      },
      { 
        id:6,
        title: 'VACCAI lección 6',
        body:'Intervalos de Séptima.',
        video:'OD1gZPWR9tI',
        link:'https://drive.google.com/open?id=1tDW4skVo4j0wy21iYnlyO3SfXfxFl2W9'
      },
      { 
        id:7,
        title: 'VACCAI lección 7',
        body:'Intervalos de Octava.',
        video:'0xvQzdqf0Mg',
        link:'https://drive.google.com/open?id=14H47DMNNMNU84tQD8EZTKcA0UXMTAprs'
      },
      { 
        id:8,
        title: 'VACCAI lección 8',
        body:'Medios Tonos, O Semi Tonos.',
        video:'cQhxc63OvMc',
        link:'https://drive.google.com/open?id=1ihZp-uz8O0_Y9EPRt4VUmnYFI94CgTO9'
      },
      { 
        id:9,
        title: 'VACCAI lección 9',
        body:'Modo Sincopado.',
        video:'j9b_Sppp0mE',
        link:'https://drive.google.com/open?id=1iz8LP8STWM_qCtemHc5ouR3GDkplHP8A'
      },
      { 
        id:10,
        title: 'VACCAI lección 10',
        body:'Introdución a Roudales.',
        video:'0xvQzdqf0Mg',
        link:'https://drive.google.com/open?id=1XIf_30KBu0hb_m-wnP39DJ3AXky3t3Va'
      },
      { 
        id:11,
        title: 'VACCAI lección 11',
        body:'La Apoyatura.',
        video:'dp-vkyMEQ4M',
        link:'https://drive.google.com/open?id=1XIf_30KBu0hb_m-wnP39DJ3AXky3t3Va'
      },
      { 
        id:12,
        title: 'VACCAI lección 12',
        body:'La Apoyatura.',
        video:'j9b_Sppp0mE',
        link:'https://drive.google.com/open?id=1XIf_30KBu0hb_m-wnP39DJ3AXky3t3Va'
      },
      { 
        id:13,
        title: 'VACCAI lección 13',
        body:'La Apoyatura.',
        video:'-zeR6zvDZes',
        link:'https://drive.google.com/open?id=1XIf_30KBu0hb_m-wnP39DJ3AXky3t3Va'
      },
    ],
    items_concone: [
      { 
        title: 'CONCONE lección 1',
        body:'',
        video2:'t_oqYTsO440',
        video:'PTROtiUR5Mg'
      },
      { 
        title: 'CONCONE lección 2',
        body:'',
        video2:'U2StI0BwFbY',
        video:'Pe7eaDAWfDE'
      },
      { 
        title: 'CONCONE lección 3',
        body:'',
        video2:'vbliCeonvZo',
        video:'_uc51E7uS9s'
      },
      { 
        title: 'CONCONE lección 4',
        body:'',
        video2:'lk4ligBscCA',
        video:'lnf-ZHYi8yU'
      },
      { 
        title: 'CONCONE lección 5',
        body:'',
        video2:'7PAxFtqfR2w',
        video:'eBcZCfYfzkY'
      },
      { 
        title: 'CONCONE lección 6',
        body:'',
        video:'r34bjT4GMcg'
      },
      { 
        title: 'CONCONE lección 7',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 8',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 9',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 10',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 11',//true
        body:'',
        video:'h9u4thzr6YQ'
      },
      { 
        title: 'CONCONE lección 12',//true
        body:'',
        video:'SdibDHptA3I'
      },
      { 
        title: 'CONCONE lección 13',//true
        body:'',
        video:'WyCfpPndYuE'
      },
      { 
        title: 'CONCONE lección 14',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 15',//true
        body:'',
        video:'kny5hDY3upw'
      },
      { 
        title: 'CONCONE lección 16',//true
        body:'',
        video:'GhXENRkze0M'
      },
      { 
        title: 'CONCONE lección 17',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 18',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 19',
        body:'',
        video:'w3rUnb6DZPY'
      },
      { 
        title: 'CONCONE lección 20',
        body:'',
        video:'w3rUnb6DZPY'
      },
    ],
    items_italiana: [
      { 
        title: 'Canción italiana nel corpiu non mi sento',
        body:'',
        video:'6ylsUTj2adI',
      },
      { 
        title: 'Canción italiana nel corpiu.',
        body:'',
        video:'5IfSF2bqYQU'
      },
      { 
        title: 'Canción italiana CARO MIO BEN ',
        body:'',
        video:'UUk8hZwkj-A'
      },
      { 
        title: 'Caro mio ben',
        body:'',
        video:'wn_dsDQyCDY'
      },
      { 
        title: 'Canción Amarili, mia bella',
        body:'',
        video:'X1TzqgzDktg'
      },
      { 
        title: 'canto o del mio dolce ardor',
        body:'',
        video:'r4VH-KA997I'
      },

      { 
        title: 'canto mio dolce ardor',
        body:'',
        video:'r4VH-KA997'
      },
      { 
        title: 'canto tu lo sai',
        body:'',
        video:'PqFQri6slh0'
      },
      { 
        title: 'Canto Che fiero costume',//false canto gia il sole dal gange
        body:'',
        video:'IFlTiB3Zb3Y'
      },
      //false Partitura cantada Vittoria, mio core
      //false Sebben Crudele partitura Kareoke
      { 
        title: 'partitura cantada alma de core',//false canto gia il sole dal gange
        body:'',
        video:'xUNofqwh_wU'
      },
      { 
        title: 'O cessate di piagarme',//false canto gia il sole dal gange
        body:'',
        video:'qGZsGBrQY4g'
      },
      { 
        title: 'Partitura y audio O cessate di piagarme',//false canto gia il sole dal gange
        body:'',
        video:'vhL1ajfvrxE'
      },
      { 
        title: 'Partitura y audio Se florindo e fedele',//false canto gia il sole dal gange
        body:'',
        video:'044SUv2mw2Q'
      },
      //false Video Se florindo e fidele
      ],
      items_sojo: [
      { 
        id:'1',
        title: 'Sojo - EN LA CAMPIÑA AMENA. guitarra.pdf',
        url:'https://drive.google.com/open?id=1HTOfRLvAOBsNzwkrXLuq7VWGOIz9pAbx',
      },
      { 
        id:'2',
        title: 'Sojo - esta noche serena. guitarra.pdf',
        url:'https://drive.google.com/open?id=1pinX_xjl4fSHYmimKeewjMmZYIFo9k2t',
      },
      { 
        id:'3',
        title: 'Sojo AL SUSURRO DE LAS PALMAS, guitarra.pdf',
        url:'https://drive.google.com/open?id=10PNKB4iRGh-kAETuD0i-8-meSOi2v3hk',
      },
      { 
        id:'4',
        title: 'Sojo cuando yo quiera casarme 2 guitarras.pdf',
        url:'https://drive.google.com/open?id=1DRsyHXmPHscHLRH8PuyHM0xK4C6bW8P3',
      },
      { 
        id:'5',
        title: 'Sojo EL VIAJERO, guitarra.pdf',
        url:'https://drive.google.com/open?id=1eTdYOV7rMvNoWDLHf2_ZJhT4ghLmcMBx',
      },
      { 
        id:'6',
        title: 'Sojo fúlgida luna.pdf',
        url:'https://drive.google.com/open?id=1sGjPFizdbabKSF1PhvClWs432B-VAdCC',
      },
      { 
        id:'7',
        title: 'Sojo fúlgida luna.pdf',
        url:'https://drive.google.com/open?id=1XkXWF1a3sJWZxK_bu6pWWwiqnktON36e',
      },
      { 
        id:'8',
        title: 'Sojo LA LUNA Y TU, guitarra.pdf',
        url:'https://drive.google.com/open?id=1qxwOX-SvHWoPzhPwCDOMdHrlLQbhlJUP',
      },
      { 
        id:'9',
        title: 'sojo NO SABES LO QUE SIENTO, guitarra.pdf',
        url:'https://drive.google.com/open?id=1_STW5AI3aC8g-HulsrYorqgmHu59N2gG',
      },
      { 
        id:'10',
        title: 'Sojo NO VAYAS A MISA ELISA, guitarra, en mi.pdf',
        url:'https://drive.google.com/open?id=1dIAWDWGkEyL24JquwOkzpOs778jU8Hb1',
      },
      { 
        id:'11',
        title: 'Sojo nunca podrás amarme, guitarra.pdf',
        url:'https://drive.google.com/open?id=1ilTrYfErjUFqVASenlQVQgYRIdw_PUXo',
      },
      { 
        id:'12',
        title: 'Sojo OH TU MI LIRIO BLANCO guitarra.pdf',
        url:'https://drive.google.com/open?id=117_wcdv23zdD1pW6AxgDP363LqTnMfxd',
      },
      { 
        id:'13',
        title: 'Sojo que no te quiera mas, guitarra.pdf',
        url:'https://drive.google.com/open?id=1Rw8kH7iug1YyClUfK9UbRaQa_RMDj69L',
      },
      { 
        id:'14',
        title: 'Sojo QUIRPA GUATIREÑA guitarra.pdf',
        url:'https://drive.google.com/open?id=1SpDPWkSr0P0VYh_6vP0SZdi04_zDCXEB',
      },
      { 
        id:'15',
        title: 'Sojo SE AGITARON, guitarrra.pdf',
        url:'https://drive.google.com/open?id=1R1e6l_BPJrbrUfqq-OUn242ocZAtVZbI',
      },
      { 
        id:'16',
        title: 'Sojo SEÑORA MÓNICA PÉREZ, guitarra.pdf',
        url:'https://drive.google.com/open?id=1jbbchBon8WkJ6JlCaJNAfAS8Pa3SAOlL',
      },
    ]

  },
  computed: {
    filteredCountVaccai: function() {
      return this.items_vaccai.slice(0, 3)
    },
    filteredCountVaccai6: function() {
      return this.items_vaccai.slice(3, 6)
    },
    filteredCountVaccai9: function() {
      return this.items_vaccai.slice(6, 9)
    },
    filteredCountVaccai12: function() {
      return this.items_vaccai.slice(9, 12)
    },
    filteredCountconcone5: function() {
      return this.items_concone.slice(0, 5)
    },
    filteredCountconcone10: function() {
      return this.items_concone.slice(5, 10)
    },
    filteredCountconcone15: function() {
      return this.items_concone.slice(10, 15)
    },
    filteredCountconcone20: function() {
      return this.items_concone.slice(15, 20)
    },
    filteredCountiitaliana0: function() {
      return this.items_italiana.slice(0, 6)
    },
    filteredCountiitaliana1: function() {
      return this.items_italiana.slice(7, 13)
    },
    filteredCounSojo0: function() {
      return this.items_sojo.slice(0, 4)
    },
    filteredCounSojo1: function() {
      return this.items_sojo.slice(4, 8)
    },
    filteredCounSojo2: function() {
      return this.items_sojo.slice(8, 12)
    },
    filteredCounSojo3: function() {
      return this.items_sojo.slice(12, 16)
    },
  }
})