const mongoose = require('mongoose'); 
const Schema = mongoose.Schema;
const User = require('./User');

var Post = new Schema({
	title:{type:String},
	picture:{type:String},
	user:{type:Schema.ObjectId,ref:'User'},
	description:{type:String},
	date:{type:Date, default:Date.now()}
},{
	collection:'items'
});

module.exports = mongoose.model('Post', Post);