const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');

var userSchema = new Schema({
	displayName:{type:String,required:true},
	email:{type:String, required:true, unique:true,lowercase:true},
	avatar:{type:String,default:''},
	password:{type:String,/*select:false,*/ required:true},
	signupDate:{type:Date, default:Date.now()},
	lastLogin:{type:Date}


});

userSchema.pre('save',function(next){
	let user = this;
	if(!this.isModified('password')) return next();
	bcrypt.genSalt(10,(err,salt)=>{
		if(err) return next();
		bcrypt.hash(user.password,salt,null,(err,hash)=>{
			if(err){
				return next(err);
			}
			else{
				user.password = hash;
				next();
			}
		})
	})
});
userSchema.methods.comparePassword = function(password){
	var user = this;
	return bcrypt.compareSync(password, user.password);

};

userSchema.methods.gravatar = function(){
	if(!this.email) return 'http//gravatar.com/avatar/?=2005d=retro';
	const md5 = crypto.createHash('md5').update(this.email).digest('hex');
	return `https//gravatar.com/avatar/${md5}?=200d=retro`;
}

module.exports = mongoose.model('userSchema',userSchema);