# Blog de Orientación Vocal


# Blog de Orientación Vocal de la Universidad Nacional Experimental Francisco de Miranda 

Blog de Orientación Vocal para entrenamiento y desarrollo de la voz.  

Contenidos programáticos exigidos en la carrera de Educación Mención Música de [UNEFM](http://www.unefm.edu.ve/)

# Instalación
blog de orientacion vocal requiere [Node.js](https://nodejs.org/) v4+ to run.

Instalar las dependencias y dependencias de desarrollo, corre el servidor

```sh
$ git clone git@bitbucket.org:edwight/proyecto-orientacion-vocal.git
$ cd proyecto-orientacion-vocal
$ npm install -d
$ npm start
```